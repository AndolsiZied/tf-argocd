module "dev-app" {
  source = "./modules/application"

  application_name   = lookup(lookup(var.params_by_env, "dev"), "application_name")
  application_url    = lookup(lookup(var.params_by_env, "dev"), "application_url")
  application_server = lookup(lookup(var.params_by_env, "dev"), "application_server")
  namespace          = var.namespace

  host                   = module.server.host
  token                  = module.server.token
  cluster_ca_certificate = module.server.cluster_ca_certificate
}

module "dev-cluster" {
  source = "./modules/cluster"

  application_cluster = lookup(lookup(var.params_by_env, "dev"), "application_cluster")
  application_project = lookup(lookup(var.params_by_env, "dev"), "application_project")
  namespace           = var.namespace

  host                   = module.server.host
  token                  = module.server.token
  cluster_ca_certificate = module.server.cluster_ca_certificate

}

module "qa-app" {
  source = "./modules/application"

  application_name   = lookup(lookup(var.params_by_env, "qa"), "application_name")
  application_url    = lookup(lookup(var.params_by_env, "qa"), "application_url")
  application_server = lookup(lookup(var.params_by_env, "qa"), "application_server")
  namespace          = var.namespace

  host                   = module.server.host
  token                  = module.server.token
  cluster_ca_certificate = module.server.cluster_ca_certificate
}

module "qa-cluster" {
  source = "./modules/cluster"

  application_cluster = lookup(lookup(var.params_by_env, "qa"), "application_cluster")
  application_project = lookup(lookup(var.params_by_env, "qa"), "application_project")
  namespace           = var.namespace

  host                   = module.server.host
  token                  = module.server.token
  cluster_ca_certificate = module.server.cluster_ca_certificate

}

module "prod-app" {
  source = "./modules/application"

  application_name   = lookup(lookup(var.params_by_env, "prod"), "application_name")
  application_url    = lookup(lookup(var.params_by_env, "prod"), "application_url")
  application_server = lookup(lookup(var.params_by_env, "prod"), "application_server")
  namespace          = var.namespace

  host                   = module.server.host
  token                  = module.server.token
  cluster_ca_certificate = module.server.cluster_ca_certificate
}

module "prod-cluster" {
  source = "./modules/cluster"

  application_cluster = lookup(lookup(var.params_by_env, "prod"), "application_cluster")
  application_project = lookup(lookup(var.params_by_env, "prod"), "application_project")
  namespace           = var.namespace

  host                   = module.server.host
  token                  = module.server.token
  cluster_ca_certificate = module.server.cluster_ca_certificate

}

module "server" {
  source = "./modules/server"

  cluster   = var.cluster
  project   = var.project
  namespace = var.namespace
}

