variable "cluster" {
  description = "The cluster name."
  type        = string
}

variable "region" {
  description = "GCP region."
  type        = string
  default     = "us-central1"
}

variable "zone" {
  description = "GCP zone"
  type        = string
  default     = "us-central1-b"
}

variable "project" {
  description = "The project ID of kubernetes cluster."
  type        = string
}

variable "namespace" {
  description = "Argocd namespace."
  type        = string
}