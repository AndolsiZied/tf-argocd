data "google_container_cluster" "cluster" {
  name     = var.cluster
  location = var.zone
  project  = var.project
}

data "google_client_config" "provider" {}

provider "kubectl" {
  host                   = "https://${data.google_container_cluster.cluster.endpoint}"
  token                  = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(data.google_container_cluster.cluster.master_auth.0.cluster_ca_certificate)
  load_config_file       = false
}

resource "kubectl_manifest" "argocd-ns" {
  yaml_body = templatefile("${path.module}/manifest/install-ns.yaml", local.server_params)
}

data "kubectl_file_documents" "manifests" {
  content = file("${path.module}/manifest/install.yaml")
}

resource "kubectl_manifest" "argocd" {
  depends_on = [
    kubectl_manifest.argocd-ns,
  ]
  count              = length(data.kubectl_file_documents.manifests.documents)
  yaml_body          = element(data.kubectl_file_documents.manifests.documents, count.index)
  override_namespace = "argocd"
}
