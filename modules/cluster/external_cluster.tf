provider "kubectl" {
  alias                  = "ext"
  host                   = "https://${data.google_container_cluster.cluster.endpoint}"
  token                  = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(data.google_container_cluster.cluster.master_auth.0.cluster_ca_certificate)
  load_config_file       = false
}

data "google_client_config" "provider" {}

data "google_container_cluster" "ext-cluster" {
  name     = var.application_cluster
  location = var.application_zone
  project  = var.application_project
}

data "kubectl_file_documents" "ext-cluster-file" {
  provider = kubectl.ext
  content  = file("${path.module}/manifest/rbac.yaml")
}

resource "kubectl_manifest" "ext-cluster-manifest" {
  provider           = kubectl.ext
  count              = length(data.kubectl_file_documents.ext-cluster-file.documents)
  yaml_body          = element(data.kubectl_file_documents.ext-cluster-file.documents, count.index)
}