variable "host" {
  description = "The hostname (in form of URI) of the Kubernetes API."
  type        = string
}

variable "token" {
  description = "Token of the service account."
  type        = string
}

variable "cluster_ca_certificate" {
  description = "PEM-encoded root certificates bundle for TLS authentication."
  type        = string
}

variable "namespace" {
  description = "Argocd namespace."
  type        = string
}

variable "application_cluster" {
  description = "The cluster name."
  type        = string
}

variable "application_region" {
  description = "GCP region."
  type        = string
  default     = "us-central1"
}

variable "application_zone" {
  description = "GCP zone"
  type        = string
  default     = "us-central1-b"
}

variable "application_project" {
  description = "The project ID of kubernetes cluster."
  type        = string
}