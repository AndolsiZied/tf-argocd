provider "kubectl" {
  host                   = var.host
  token                  = var.token
  cluster_ca_certificate = var.cluster_ca_certificate
  load_config_file       = false
}

provider "kubernetes" {
  host = "https://${data.google_container_cluster.cluster.endpoint}"

  token                  = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(data.google_container_cluster.cluster.master_auth.0.cluster_ca_certificate)
}

data "google_container_cluster" "cluster" {
  name     = var.application_cluster
  location = var.application_zone
  project  = var.application_project
}

data "kubernetes_service_account" "argocd-manager" {
  metadata {
    name = "argocd-manager"
    namespace = "kube-system"
  }
  depends_on = [
    kubectl_manifest.ext-cluster-manifest,
  ]
}

data "kubernetes_secret" "argocd-manager" {
  metadata {
    name = data.kubernetes_service_account.argocd-manager.default_secret_name
    namespace = "kube-system"
  }
}

resource "kubectl_manifest" "cluster-manifest" {
  yaml_body          = templatefile("${path.module}/manifest/secret.yaml", local.cluster_params)
  override_namespace = var.namespace
}