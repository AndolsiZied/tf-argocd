locals {
  cluster_params = {
    cluster_name           = var.application_cluster
    cluster_url            = "https://${data.google_container_cluster.cluster.endpoint}"
    cluster_token          = data.kubernetes_secret.argocd-manager.data.token
    cluster_ca_certificate = base64encode(lookup(data.kubernetes_secret.argocd-manager.data, "ca.crt"))
  }
}