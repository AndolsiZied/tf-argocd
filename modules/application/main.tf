provider "kubectl" {
  host                   = var.host
  token                  = var.token
  cluster_ca_certificate = var.cluster_ca_certificate
  load_config_file       = false
}

data "kubectl_file_documents" "app-file" {
  content = templatefile("${path.module}/manifest/install.yaml", local.app_params)
}

resource "kubectl_manifest" "app-manifest" {
  count              = length(data.kubectl_file_documents.app-file.documents)
  yaml_body          = element(data.kubectl_file_documents.app-file.documents, count.index)
  override_namespace = var.namespace
}