locals {
  app_params = {
    application_name   = var.application_name
    application_url    = var.application_url
    application_server = var.application_server
  }
}