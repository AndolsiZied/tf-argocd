variable "host" {
  description = "The hostname (in form of URI) of the Kubernetes API."
  type        = string
}

variable "token" {
  description = "Token of the service account."
  type        = string
}

variable "cluster_ca_certificate" {
  description = "PEM-encoded root certificates bundle for TLS authentication."
  type        = string
}

variable "namespace" {
  description = "Argocd namespace."
  type        = string
}

variable "application_name" {
  description = "The application's name."
  type        = string
}

variable "application_url" {
  description = "The application's url."
  type        = string
}

variable "application_server" {
  description = "The application's server."
  type        = string
}