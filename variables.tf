variable "cluster" {
  description = "The cluster name."
  type        = string
}

variable "region" {
  description = "GCP region."
  type        = string
  default     = "us-central1"
}

variable "project" {
  description = "The project ID of kubernetes cluster."
  type        = string
}

variable "namespace" {
  description = "Argocd namespace."
  type        = string
  default     = "argocd"
}

variable "params_by_env" {
  description = "A configuration of application and cluster object."
  type        = map(any)
}