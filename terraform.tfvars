cluster = "transverse-k8s-cluster"
project = "tf-root-pj"
params_by_env = {
  dev = {
    application_name    = "cd-sample-python-dev"
    application_url     = "https://gitlab.com/AndolsiZied/cd-sample-python-dev.git"
    application_server  = "https://34.70.237.101"
    application_cluster = "dev-app-k8s-cluster"
    application_project = "tf-dev-infra-stack"
  },
  qa = {
    application_name    = "cd-sample-python-qa"
    application_url     = "https://gitlab.com/AndolsiZied/cd-sample-python-qa.git"
    application_server  = "https://35.225.213.63"
    application_cluster = "qa-app-k8s-cluster"
    application_project = "tf-qa-infra-stack"
  },
  prod = {
    application_name    = "cd-sample-python-prod"
    application_url     = "https://gitlab.com/AndolsiZied/cd-sample-python-prod.git"
    application_server  = "https://35.238.69.96"
    application_cluster = "prod-app-k8s-cluster"
    application_project = "tf-prod-infra-stack"
  }
}